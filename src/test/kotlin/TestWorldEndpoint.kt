import org.junit.jupiter.api.assertAll
import org.soullink.ApiClient
import org.soullink.schema.general.Granularity
import kotlin.test.Test

class TestWorldEndpoint {
    private val applicationKey = System.getenv("WA_APPLICATION_KEY")
    private val userApiKey = System.getenv("WA_USER_API_KEY")

    @Test
    fun testWorlds() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val list = client.user.listWorlds(userId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    fun testGet() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first().id
        val world = client.world.get(worldId, Granularity.REFERENCE)
        val world0 = client.world.get(worldId, Granularity.BASE)
        println(world0)
        val world1 = client.world.get(worldId, Granularity.ENTITY)
        val world2 = client.world.get(worldId, Granularity.EXTENDED)
        val world3 = client.world.get(worldId, Granularity.ALL)
        assertAll(
            { assert(world.id.isNotEmpty()) },
            { assert(world.title.isNotEmpty()) },
            { assert(world0.id.isNotEmpty()) },
            { assert(world0.title.isNotEmpty()) },
            { assert(world1.id.isNotEmpty()) },
            { assert(world1.title.isNotEmpty()) },
            { assert(world2.id.isNotEmpty()) },
            { assert(world2.title.isNotEmpty()) },
            { assert(world3.id.isNotEmpty()) },
            { assert(world3.title.isNotEmpty()) }
        )
    }



    @Test
    fun testListArticles() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first().id
        val list = client.world.listArticles(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    fun testListSecrets() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first { it.title == "Aurelia" }.id
        val list = client.world.listSecrets(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    fun testListVariableCollections() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first { it.title == "Aurelia" }.id
        val list = client.world.listVariableCollections(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }
    @Test
    fun testListSubscribergroups() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first { it.title == "Aurelia" }.id
        val list = client.world.listSubscriberGroups(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    fun testListManuscripts() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first { it.title == "World Behind the Veil" }.id
        val list = client.world.listManuscripts(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    fun testListTimelines() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first { it.title == "World Behind the Veil" }.id
        val list = client.world.listTimelines(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }
    @Test
    fun testListHistories() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first { it.title == "World Behind the Veil" }.id
        val list = client.world.listHistories(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }
    @Test
    fun testListCategories() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first { it.title == "World Behind the Veil" }.id
        val list = client.world.listCategories(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    fun testListCanvases() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first { it.title == "World Behind the Veil" }.id
        val list = client.world.listCanvases(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    fun testListBlockFolders() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first { it.title == "Aurelia" }.id
        val list = client.world.listBlockFolders(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    fun testListImages() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first { it.title == "Aurelia" }.id
        val list = client.world.listImages(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    fun testListMaps() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worldId = client.user.listWorlds(userId).entities.first { it.title == "Aurelia" }.id
        val list = client.world.listMaps(worldId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

}