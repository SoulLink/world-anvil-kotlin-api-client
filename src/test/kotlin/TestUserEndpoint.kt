import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.assertAll
import org.soullink.ApiClient
import org.soullink.schema.general.Granularity
import org.soullink.schema.user.UserUpdate
import kotlin.test.Test

class TestUserEndpoint {

    private val applicationKey = System.getenv("WA_APPLICATION_KEY")
    private val userApiKey = System.getenv("WA_USER_API_KEY")

    init {
        if (applicationKey == null || userApiKey == null) {
            throw Exception("Please set WA_APPLICATION_KEY and WA_USER_API_KEY environment variables")
        }
    }


    @Test
    fun testIdentity() {
        val client = ApiClient(applicationKey, userApiKey)
        val identity = client.user.identity()
        assertAll(
            { assert(identity.id.isNotEmpty()) },
            { assert(identity.username.isNotEmpty()) },
            { assert(identity.userhash.isNotEmpty()) },
            { assert(identity.success) }
        )
    }

    @Test
    fun testGet() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val user = client.user.get(userId, Granularity.REFERENCE)
        val user0 = client.user.get(userId, Granularity.BASE)
        val user1 = client.user.get(userId, Granularity.ENTITY)
        val user2 = client.user.get(userId, Granularity.EXTENDED)
        val user3 = client.user.get(userId, Granularity.ALL)
        println(user)
        assertAll(
            { assert(user.id.isNotEmpty()) },
            { assert(user0.id.isNotEmpty()) },
            { assert(user1.id.isNotEmpty()) },
            { assert(user2.id.isNotEmpty()) },
            { assert(user3.id.isNotEmpty()) },
        )
    }

    @Test
    fun testWorlds() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val list = client.user.listWorlds(userId)
        println(list)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    fun testBlockTemplates() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val list = client.user.listBlockTemplates(userId)
        println(list)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    fun testListNotebooks() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val list = client.user.listNotebooks(userId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }

    @Test
    @Disabled
    fun testPatch() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val user = client.user.get(userId, Granularity.ALL)
        val updatedUser = client.user.update(
            userId, UserUpdate(
                firstname = user.firstname,
                username = user.username,
                lastname = user.lastname,
                email = user.email,
                bio = user.bio,
                locale = user.locale,
                primaryUseType = user.primaryUseType,
                onboardingProgress = user.onboardingProgress,
                signature = user.signature,
                customProfileContent = user.customProfileContent,
                membershipType = user.membershipType,
                websiteUrl = user.websiteUrl,
                favMovies = user.favMovies,
                favSeries = user.favSeries,
                favBooks = user.favBooks,
                favWriters = user.favWriters,
                favGames = user.favGames,
                interests = user.interests,
                nanowrimo = user.nanowrimo,
                twitter = user.twitter,
                facebook = user.facebook,
                reddit = user.reddit,
                tumblr = user.tumblr,
                pinterest = user.pinterest,
                deviantart = user.deviantart,
                youtube = user.youtube,
                vimeo = user.vimeo,
                google = user.google,
                steam = user.steam,
                twitch = user.twitch,
                discord = user.discord,
                instagram = user.instagram,
                kofi = user.kofi,
                patreon = user.patreon,
                mastodon = user.mastodon,
                bluesky = user.bluesky,
                views = user.views,
                openText = user.openText,
                profileMeta = user.profileMeta,
                timezone = user.timezone,
                location = user.location,
                dob = user.dob,
                interfaceFormFontSize = user.interfaceFormFontSize,
                interfaceVignetteRows = user.interfaceVignetteRows,
                interfaceActivateAccessibility = user.interfaceActivateAccessibility,
                interfaceActivateAdvancedSelect = user.interfaceActivateAdvancedSelect,
                featureCommunity = user.featureCommunity,
                interfaceVersion = user.interfaceVersion,
                interfaceEditorMode = user.interfaceEditorMode,
                membershipPrototype = user.membershipPrototype,
                featureWorldbuilding = user.featureWorldbuilding,
                interfaceEditorTheme = user.interfaceEditorTheme,
                allowAdultContent = user.allowAdultContent,
                interfaceFormColor = user.interfaceFormColor,
                manuscriptSettings = user.manuscriptSettings,
                featurePrompts = user.featurePrompts,
                featureWriting = user.featureWriting,
                interfaceTheme = user.interfaceTheme,
                featureAutosave = user.featureAutosave,
                interfaceEditor =  user.interfaceEditor,
                subscriberSlots = user.subscriberSlots,
                chapterhouse = user.chapterhouse,
                isCompetitor = user.isCompetitor,
                isNewsletter = user.isNewsletter,
                featureHeroes = user.featureHeroes,
                featureExpandedArticle = user.featureExpandedArticle,
                interfaceFormBackground = user.interfaceFormBackground,
                featureRPG = user.featureRPG,
                interfaceShowSaveIndicator = user.interfaceShowSaveIndicator,
                cover = user.cover,
                avatar = user.avatar,
                )
        )
        println(updatedUser)
        assertAll(
            { assert(updatedUser.id.isNotEmpty()) },
        )
    }

}