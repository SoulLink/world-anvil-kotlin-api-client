import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.assertAll
import org.soullink.ApiClient
import kotlin.test.Test

class TestSubscriberGroupEndpoint {
    private val applicationKey = System.getenv("WA_APPLICATION_KEY")
    private val userApiKey = System.getenv("WA_USER_API_KEY")
    private val userUuid = System.getenv("WA_USER_UUID")
    private val userUuid2 = System.getenv("WA_USER_UUID2")
    private val testWorldUuid = System.getenv("WA_TEST_WORLD_UUID")

    @Test
    fun testSubscriberGroupLifeCycle() {
        val client = ApiClient(applicationKey, userApiKey)
        val subscriberGroup = client.subscriberGroup.create(
            title = "Test Subscriber Group",
            worldId = testWorldUuid,
            position = 1000,
            description = "Test description",
            isDefault = true,
            isAssignable = true,
            isHidden = true,
        )
        assertAll(
            { assertThat(subscriberGroup.id)
                .isNotEmpty()
            },
            { assertThat(subscriberGroup.title).isEqualTo("Test Subscriber Group") },
            { assertThat(subscriberGroup.state).isNull() },
            { assertThat(subscriberGroup.subscriberGroups).isEmpty() },
        )
        try {
            val subscriberGroupBase = client.subscriberGroup.getBase(subscriberGroup.id)
            assertAll(
                { assertThat(subscriberGroupBase.position).isEqualTo(1000) },
                { assertThat(subscriberGroupBase.description).isEqualTo("Test description") },
                { assertThat(subscriberGroupBase.isDefault).isTrue() },
            )

            val subscriberGroupEntity = client.subscriberGroup.get(subscriberGroup.id)
            assertAll(
                { assertThat(subscriberGroupEntity.isAssignable).isTrue() },
                { assertThat(subscriberGroupEntity.isHidden).isTrue() },
                { assertThat(subscriberGroupEntity.campaign).isNull() },
                { assertThat(subscriberGroupEntity.party).isNull() },
            )

            client.subscriberGroup.update(subscriberGroup.id, paidSubscribers = listOf(userUuid, userUuid2))

            val subscriberGroupExtended = client.subscriberGroup.getExtended(subscriberGroup.id)
            assertAll(
                { assertThat(subscriberGroupBase.position).isEqualTo(1000) },
                { assertThat(subscriberGroupBase.description).isEqualTo("Test description") },
                { assertThat(subscriberGroupBase.isDefault).isTrue() },
                { assertThat(subscriberGroupExtended.paidsubscribers).size().isEqualTo(2) },
                { assertThat(subscriberGroupEntity.isAssignable).isTrue() },
                { assertThat(subscriberGroupEntity.isHidden).isTrue() },
                { assertThat(subscriberGroupEntity.campaign).isNull() },
                { assertThat(subscriberGroupEntity.party).isNull() },
            )
            client.subscriberGroup.update(subscriberGroup.id, paidSubscribers = listOf())

            val subscriberGroupExtended2 = client.subscriberGroup.getExtended(subscriberGroup.id)
            assertAll(
                { assertThat(subscriberGroupExtended2.paidsubscribers).size().isEqualTo(0) },
            )
        } catch (ex: Exception) {
            ex.printStackTrace()
        } finally {
            val deleted = client.subscriberGroup.delete(subscriberGroup.id)
            assertAll(
                { assert(deleted.success) }
            )
        }




    }
}