import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.assertAll
import org.soullink.ApiClient
import org.soullink.schema.general.Granularity
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.articles.enums.ArticleTemplate
import kotlin.test.Test

class TestArticleEndpoint {
    private val applicationKey = System.getenv("WA_APPLICATION_KEY")
    private val userApiKey = System.getenv("WA_USER_API_KEY")
    private val testWorldUuid = System.getenv("WA_TEST_WORLD_UUID")
    private val testCategory = System.getenv("WA_TEST_CATEGORY")

    @Test
    fun testWorlds() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val list = client.user.listWorlds(userId)
        assertAll(
            { assert(list.success) },
            { assert(list.entities.isNotEmpty()) }
        )
    }


    @Test
    fun articleLifeCycle() {
        val client = ApiClient(applicationKey, userApiKey)

        val article = client.article.create(testWorldUuid, "Test Article", ArticleTemplate.ARTICLE)
        assertAll(
            { assert(article.id.isNotEmpty()) },
            { assert(article.title == "Test Article") },
            { assert(article.state == PrivacyState.PUBLIC) },
        )

        val updatedArticle = client.article.update(article.id, "Updated Test Article")
        assertAll(
            { assert(updatedArticle.id.isNotEmpty()) },
            { assert(updatedArticle.title == "Updated Test Article") }
        )
        val deletedArticle = client.article.delete(updatedArticle.id)
        assertAll(
            { assert(deletedArticle.success) }
        )

    }

    @Test
    @Disabled
    fun testGet() {
        val client = ApiClient(applicationKey, userApiKey)
        val userId = client.user.identity().id
        val worlds = client.user.listWorlds(userId).entities
        val worldId = worlds.first().id
        val articles = client.world.listArticles(worldId)
        for (article in articles.entities) {
//            val article0 = client.article.get(article.id, Granularity.REFERENCE)
//            val article1 = client.article.get(article.id, Granularity.BASE)
//            val article2 = client.article.get(article.id, Granularity.ENTITY)
//            val article3 = client.article.get(article.id, Granularity.EXTENDED)
            val article4 = client.article.get(article.id, Granularity.ALL)
            assertAll(
//                { assert(article0.id.isNotEmpty()) },
//                { assert(article0.title.isNotEmpty()) },
//                { assert(article1.id.isNotEmpty()) },
//                { assert(article1.title.isNotEmpty()) },
//                { assert(article2.id.isNotEmpty()) },
//                { assert(article2.title.isNotEmpty()) },
//                { assert(article3.id.isNotEmpty()) },
//                { assert(article3.title.isNotEmpty()) },
                { assert(article4.id.isNotEmpty()) },
                { assert(article4.title.isNotEmpty()) }
            )
        }
    }
}