import org.junit.jupiter.api.assertAll
import org.soullink.ApiClient
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.general.ResourceId
import org.soullink.schema.subscribergroup.SubscriberGroupReference
import kotlin.test.Test

class TestSecretEndpoint {
    private val applicationKey = System.getenv("WA_APPLICATION_KEY")
    private val userApiKey = System.getenv("WA_USER_API_KEY")
    private val testWorldUuid = System.getenv("WA_TEST_WORLD_UUID")
    private val testCategory = System.getenv("WA_TEST_CATEGORY")


    @Test
    fun testSecretLifecycle() {
        val client = ApiClient(applicationKey, userApiKey)
        val secret = client.secret.create(
            worldId = testWorldUuid,
            title = "Test Secret",
            content = "Some Content",
            cssClasses = "special death",
            subscriberGroupIds = emptyList(),
            tags = "tag1,tag2,tag3",
            articleId = null,
            authorId = null,
            seeded = "Some Seed",
            state = PrivacyState.PRIVATE,)
        assertAll(
            { assert(secret.id.isNotEmpty()) },
            { assert(secret.title == "Test Secret") },
            { assert(secret.state == PrivacyState.PRIVATE) },
            { assert(secret.subscriberGroups == emptyList<SubscriberGroupReference>()) },
        )
        try {

            val updatedSecret = client.secret.update(secret.id, "Updated Test Secret")
            assertAll(
                { assert(updatedSecret.id == secret.id) },
                { assert(updatedSecret.title == "Updated Test Secret") }
            )
            val secretRef = client.secret.getRef(secret.id)
            val secretBase = client.secret.getBase(secret.id)
            val secretExtended = client.secret.getExtended(secret.id)
        } catch (ex: Exception) {
            ex.printStackTrace()
        } finally {
            val deletedArticle = client.secret.delete(secret.id)
            assertAll(
                { assert(deletedArticle.success) }
            )
        }




    }
}