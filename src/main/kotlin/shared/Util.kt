package org.soullink.shared

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.soullink.exceptions.WorldAnvilException
import org.soullink.schema.general.ListRequestBody
import org.soullink.schema.general.UnprocessableEntity

object Util {


    const val BASE_URL = "https://www.worldanvil.com/api/external/boromir"
    const val UNAUTHORIZED_ERROR_MESSAGE =
        "Unauthorized access. The provided token or application key is invalid."
    const val FORBIDDEN_ERROR_MESSAGE =
        "Forbidden access. The provided authorization token does not have permission to access this resource."


    val applicationJson = "application/json".toMediaType()

    fun limitRequest(limit: Int, offset: Int): RequestBody {
        if (limit < 1 || limit > 50) {
            throw WorldAnvilException("Limit must be greater than 0 and less than or equal to 50.")
        }
        if (offset < 0) {
            throw WorldAnvilException("Offset must be greater than or equal to 0")
        }
        return Json.encodeToString(ListRequestBody(limit, offset)).toRequestBody(applicationJson)
    }

    fun <T> executeRequest(request: okhttp3.Request, httpClient: OkHttpClient, parser: (String) -> T): T {
        httpClient.newCall(request).execute().use { response ->
            val body = response.body ?: throw WorldAnvilException("Empty response body")
            val responseBody = body.string()
            when (response.code) {
                200 -> return parser(responseBody)
                401 -> throw WorldAnvilException(UNAUTHORIZED_ERROR_MESSAGE)
                403 -> throw WorldAnvilException(FORBIDDEN_ERROR_MESSAGE)
                404 -> throw WorldAnvilException("The provided id does not exist.")
                422 -> {
                    val entity = Json.decodeFromString(UnprocessableEntity.serializer(), responseBody)
                    throw WorldAnvilException("The request could not be processed.\n${entity.error.summary}")
                }
                else -> throw WorldAnvilException("An unexpected error occurred. Response code: ${response.code}")
            }
        }
    }
}