package org.soullink.exceptions

class WorldAnvilException(message: String) : Exception(message)