package org.soullink.schema.subscribergroup

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.soullink.schema.general.ResourceId
import org.soullink.shared.Util

@Serializable
data class SubscriberGroupCreate(
    val title: String,
    val world: ResourceId,
    val paidsubscribers: List<ResourceId>? = null,
    val description: String? = null,
    val position: Int? = null,
    val isDefault: Boolean? = null,
    val isHidden: Boolean? = null,
    val isAssignable: Boolean? = null,
) {
    fun toRequestBody(): RequestBody {
        return Json.encodeToString(serializer(), this)
            .toRequestBody(Util.applicationJson)
    }
}