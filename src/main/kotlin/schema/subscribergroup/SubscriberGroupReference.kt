package org.soullink.schema.subscribergroup

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.soullink.schema.general.EntityClass
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.general.ResourceReference

@Serializable
data class SubscriberGroupReference(
    val id: String,
    val title: String,
    val slug: String?,
    val state: PrivacyState?,
    val entityClass: EntityClass,
    val icon: String?,
    val url: String?,
    @SerialName("subscribergroups") val subscriberGroups: List<ResourceReference>,
)

