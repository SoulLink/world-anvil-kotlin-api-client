package org.soullink.schema.subscribergroup

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.soullink.schema.general.*
import org.soullink.schema.world.WorldReference

@Serializable
data class SubscriberGroupEntity(
    val id: String,
    val title: String,
    val slug: String?,
    val state: PrivacyState?,
    val isWip: Boolean?,
    val isDraft: Boolean?,
    val entityClass: EntityClass,
    val icon: String?,
    val url: String?,
    @SerialName("subscribergroups") val subscriberGroups: List<ResourceReference>,
    val folderId: String?,
    val tags: String?,
    val updateDate: Date?,

    val description: String?,
    val position: Int?,
    val isDefault: Boolean,

    val creationDate: Date,
    val isHidden: Boolean,
    val isAssignable: Boolean,
    val campaign: ResourceReference?,
    val party: ResourceReference?,
    val world: WorldReference,


    val isEditable: Boolean,
    val success: Boolean,
)