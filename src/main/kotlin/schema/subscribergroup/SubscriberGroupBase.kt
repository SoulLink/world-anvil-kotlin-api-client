package org.soullink.schema.subscribergroup

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.soullink.schema.general.Date
import org.soullink.schema.general.EntityClass
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.general.ResourceReference

@Serializable
data class SubscriberGroupBase(
    val id: String,
    val title: String,
    val slug: String?,
    val state: PrivacyState?,
    val isWip: Boolean?,
    val isDraft: Boolean?,
    val entityClass: EntityClass,
    val icon: String?,
    val url: String?,
    @SerialName("subscribergroups") val subscriberGroups: List<ResourceReference>,
    val folderId: String?,
    val tags: String?,
    val updateDate: Date?,

    val description: String?,
    val position: Int?,
    val isDefault: Boolean,

    val isEditable: Boolean,
    val success: Boolean,
)