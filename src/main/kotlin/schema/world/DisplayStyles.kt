package org.soullink.schema.world

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DisplayStyles(
    @SerialName("display_background_id")
    val displayBackgroundId: String? = null,
    @SerialName("display_background")
    val displayBackground: String? = null,
    @SerialName("display_base_font_color")
    val displayBaseFontColor: String? = null,
    @SerialName("display_base_font_size")
    val displayBaseFontSize: String? = null,
)
