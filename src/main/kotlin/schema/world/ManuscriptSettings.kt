package org.soullink.schema.world

import kotlinx.serialization.Serializable


@Serializable
data class ManuscriptSettings(
    val fontSize: String,
    val lineHeight: String,
    val paragraphyPadding: String,
    val cssRules: String,
    val fontTypeface: String,
    val paragraphIndent: String,
    val backgroundColor: String,
    val fontColor: String,
    val paragraphPadding: String,
)