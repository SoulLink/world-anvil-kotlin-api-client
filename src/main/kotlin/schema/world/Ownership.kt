package org.soullink.schema.world

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class Ownership {
    @SerialName("owner")
    OWNER,
}