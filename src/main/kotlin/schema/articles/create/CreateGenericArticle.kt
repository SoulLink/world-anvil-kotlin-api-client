package org.soullink.schema.articles.create

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.soullink.schema.general.ResourceIntId
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.general.ResourceId
import org.soullink.schema.articles.enums.ArticleTemplate
import org.soullink.shared.Util

@Serializable
data class CreateGenericArticle(
    val title: String,
    val templateType: ArticleTemplate,
    val world: ResourceId,

    val state: PrivacyState? = null,
    val isWip: Boolean? = null,
    val isDraft: Boolean? = null,
    val icon: String? = null,
    val tags: String? = null,


    val position: Int? = null,
    val excerpt: String? = null,
    val cssClasses: String? = null,
    val displayCss: String? = null,
    val pronunciation: String? = null,
    val seeded: String? = null,
    val scrapbook: String? = null,
    val snippet: String? = null,
    val sidebarcontent: String? = null,
    val sidepanelcontenttop: String? = null,
    val sidepanelcontent: String? = null,
    val sidebarcontentbottom: String? = null,
    val footnotes: String? = null,
    val fullfooter: String? = null,
    val authornotes: String? = null,
    val metaTitle: String? = null,
    val metaDescription: String? = null,
    val subheading: String? = null,
    val credits: String? = null,
    val displaySidebar: Boolean? = null,
    val showSeeded: Boolean? = null,
    val webhookUpdate: Boolean? = null,
    val communityUpdate: Boolean? = null,
    val coverIsMap: Boolean? = null,
    val isFeaturedArticle: Boolean? = null,
    val isAdultContent: Boolean? = null,
    val allowComments: Boolean? = null,
    val showInToc: Boolean? = null,
    val isEmphasized: Boolean? = null,
    val displayAuthor: Boolean? = null,
    val displayChildrenUnder: Boolean? = null,
    val displayTitle: Boolean? = null,
    val displaySheet: Boolean? = null,
    val manuscripts: List<ResourceId>? = null,
    val block: ResourceIntId? = null,
    val articleParent: ResourceId? = null,
    val gallery: ResourceIntId? = null,
    val articlePrevious: ResourceId? = null,
    val articleNext: ResourceId? = null,
    val cover: ResourceIntId? = null,
    val category: ResourceId? = null,
    val author: ResourceId? = null,
    val subscribergroups: List<ResourceId>? = null,
) {
    fun toRequestBody(): RequestBody {
        return Json.encodeToString(serializer(), this)
            .toRequestBody(Util.applicationJson)
    }
}