package org.soullink.schema.articles.update

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.soullink.shared.Util

@Serializable
data class UpdateGenericArticle(
    val title: String
) {

    fun toRequestBody(): RequestBody {
        return Json.encodeToString(serializer(), this)
            .toRequestBody(Util.applicationJson)
    }
}