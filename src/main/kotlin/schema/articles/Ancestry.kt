package org.soullink.schema.articles

import kotlinx.serialization.Serializable
import org.soullink.schema.general.ResourceReference

@Serializable
data class Ancestry(
    val firstUp: ResourceReference,
    val secondUp: ResourceReference?,
    val thirdUp: ResourceReference?,
)
