package org.soullink.schema.articles

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.soullink.schema.general.Date
import org.soullink.schema.general.EntityClass
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.general.ResourceReference
import org.soullink.schema.image.Image
import org.soullink.schema.subscribergroup.SubscriberGroupReference
import org.soullink.schema.user.UserReference
import org.soullink.schema.world.WorldReference

@Serializable
sealed class Base(
    val entityClass: EntityClass,
) {
    // Reference fields
    abstract val id: String
    abstract val title: String
    abstract val slug: String
    abstract val state: PrivacyState
    abstract val isWip: Boolean?
    abstract val isDraft: Boolean?
    abstract val icon: String
    abstract val url: String
    @SerialName("subscribergroups")
    abstract val subscriberGroups: List<SubscriberGroupReference>
    abstract val folderId: String?
    abstract val tags: String
    abstract val updateDate: Date
    // API fields
    abstract val isEditable: Boolean
    abstract val success: Boolean

    // Social fields
    abstract val likes: Int?
    abstract val views: Int?
    abstract val comments: Int?

    abstract val commentPlaceholder: String?

    // Design fields
    abstract val cssClasses: String?
    abstract val displayCss: String?
    abstract val cover: Image?
    abstract val snippet: String?

    // Navigation fields
    abstract val category: ResourceReference?
    abstract val articleNext: ArticleReference?
    abstract val articlePrevious: ArticleReference?
    abstract val articleParent: ArticleReference?
    abstract val ancestry: Ancestry?


    // Generic Article Template fields
    abstract val content: String?
    abstract val excerpt: String?
    abstract val pronunciation: String?
    abstract val seeded: String?
    abstract val sidebarcontent: String?
    abstract val sidepanelcontenttop: String?
    abstract val sidepanelcontent: String?
    abstract val sidebarcontentbottom: String?
    abstract val footnotes: String?
    abstract val fullfooter: String?
    abstract val authornotes: String?
    abstract val credits: String?
    abstract val displaySidebar: Boolean?
    abstract val subheading: String?

    // Generic Relationship fields
    abstract val timeline: ResourceReference?
    abstract val prompt: ResourceReference?
    abstract val gallery: ResourceReference?
    abstract val block: ResourceReference?
    abstract val orgchart: ResourceReference?

    // Preferences
    abstract val showSeeded: Boolean?
    abstract val webhookUpdate: Boolean?
    abstract val communityUpdate: Boolean?
    abstract val coverIsMap: Boolean?
    abstract val isFeaturedArticle: Boolean?
    abstract val isAdultContent: Boolean?
    abstract val allowComments: Boolean?
    abstract val showInToc: Boolean?
    abstract val isEmphasized: Boolean?
    abstract val displayAuthor: Boolean?
    abstract val displayChildrenUnder: Boolean?
    abstract val displayTitle: Boolean?
    abstract val displaySheet: Boolean?

    // Meta fields
    abstract val templateType: String?
    abstract val customArticleTemplate: String?
    abstract val author: UserReference?
    abstract val world: WorldReference?
    abstract val passcodecta: String?
    abstract val metaTitle: String?
    abstract val metaDescription: String?
    abstract val scrapbook: String?
    abstract val userMetadata: String?
    abstract val articleMetadata: String?
    abstract val position: Int?
    abstract val wordcount: Int?
    abstract val creationDate: Date?
    abstract val publicationDate: Date?
    abstract val notificationDate: Date?
    abstract val isLocked: Boolean?
    abstract val badge: ResourceReference?
    abstract val editURL: String?
}


