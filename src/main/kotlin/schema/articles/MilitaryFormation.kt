package org.soullink.schema.articles

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.soullink.schema.general.Date
import org.soullink.schema.general.EntityClass
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.general.ResourceReference
import org.soullink.schema.image.Image
import org.soullink.schema.subscribergroup.SubscriberGroupReference
import org.soullink.schema.user.UserReference
import org.soullink.schema.world.WorldReference

@Serializable
@SerialName("MilitaryFormation")
data class MilitaryFormation(

    val person: ResourceReference?,
    val vehicle: ResourceReference?,
    val rank: ResourceReference?,
    override val id: String,
    override val title: String,
    override val slug: String,
    override val state: PrivacyState,
    override val isWip: Boolean?,
    override val isDraft: Boolean?,
    override val icon: String,
    override val url: String,
    @SerialName("subscribergroups")
    override val subscriberGroups: List<SubscriberGroupReference>,
    override val folderId: String?,
    override val tags: String,
    override val updateDate: Date,
    override val isEditable: Boolean,
    override val success: Boolean,
    override val likes: Int?,
    override val views: Int?,
    override val comments: Int?,
    override val commentPlaceholder: String?,
    override val cssClasses: String?,
    override val displayCss: String?,
    override val cover: Image?,
    override val snippet: String?,
    override val category: ResourceReference?,
    override val articleNext: ArticleReference?,
    override val articlePrevious: ArticleReference?,
    override val articleParent: ArticleReference?,
    override val ancestry: Ancestry?,
    override val content: String?,
    override val excerpt: String?,
    override val pronunciation: String?,
    override val seeded: String?,
    override val sidebarcontent: String?,
    override val sidepanelcontenttop: String?,
    override val sidepanelcontent: String?,
    override val sidebarcontentbottom: String?,
    override val footnotes: String?,
    override val fullfooter: String?,
    override val authornotes: String?,
    override val credits: String?,
    override val displaySidebar: Boolean?,
    override val subheading: String?,
    override val timeline: ResourceReference?,
    override val prompt: ResourceReference?,
    override val gallery: ResourceReference?,
    override val block: ResourceReference?,
    override val orgchart: ResourceReference?,
    override val showSeeded: Boolean?,
    override val webhookUpdate: Boolean?,
    override val communityUpdate: Boolean?,
    override val coverIsMap: Boolean?,
    override val isFeaturedArticle: Boolean?,
    override val isAdultContent: Boolean?,
    override val allowComments: Boolean?,
    override val showInToc: Boolean?,
    override val isEmphasized: Boolean?,
    override val displayAuthor: Boolean?,
    override val displayChildrenUnder: Boolean?,
    override val displayTitle: Boolean?,
    override val displaySheet: Boolean?,
    override val templateType: String?,
    override val customArticleTemplate: String?,
    override val author: UserReference?,
    override val world: WorldReference?,
    override val passcodecta: String?,
    override val metaTitle: String?,
    override val metaDescription: String?,
    override val scrapbook: String?,
    override val userMetadata: String?,
    override val articleMetadata: String?,
    override val position: Int?,
    override val wordcount: Int?,
    override val creationDate: Date?,
    override val publicationDate: Date?,
    override val notificationDate: Date?,
    override val isLocked: Boolean?,
    override val badge: ResourceReference?,
    override val editURL: String?,
) : Base(EntityClass.RANK)