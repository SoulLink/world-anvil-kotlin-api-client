package org.soullink.schema.articles.enums

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class ArticleTemplate {
    @SerialName("article")
    ARTICLE,
}