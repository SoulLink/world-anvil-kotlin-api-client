package org.soullink.schema.timeline

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.general.ResourceId
import org.soullink.schema.subscribergroup.SubscriberGroupReference
import org.soullink.schema.general.Date
import org.soullink.schema.general.EntityClass

@Serializable
data class TimelineReference(
    val id: String,
    val title: String,
    val slug: String?,
    val state: PrivacyState?,
    val isWip: Boolean?,
    val isDraft: Boolean?,
    val entityClass: EntityClass,
    val icon: String?,
    val url: String?,
    @SerialName("subscribergroups")
    val subscriberGroups: List<SubscriberGroupReference>,
    val folderId: Int?,
    val tags: String?,
    val updateDate: Date?,
    val version: TimelineVersion,
    val isEditable: Boolean = false,
    val success: Boolean = false,
) {
    fun resourceId() = ResourceId(id)
}

enum class TimelineVersion {
    @SerialName("horae")
    HORAE,
}