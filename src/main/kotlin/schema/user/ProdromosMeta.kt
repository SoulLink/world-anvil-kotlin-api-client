package org.soullink.schema.user

import kotlinx.serialization.Serializable
import org.soullink.schema.general.Date

@Serializable
data class ProdromosMeta(
    val communityFeedback: CommunityFeedback
)

@Serializable
data class CommunityFeedback(
    val lastDissmissed: Date
)
