package org.soullink.schema.user

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.soullink.schema.general.EntityClass
import org.soullink.schema.subscribergroup.SubscriberGroupReference
import org.soullink.schema.general.Date

@Serializable
data class Badge(
    val id: Int,
    val title: String,
    val slug: String? = null,
    val state: String? = null,
    val isWip: Boolean? = null,
    val isDraft: Boolean? = null,
    val entityClass: EntityClass = EntityClass.BADGE,
    val icon: String,
    val url: String? = null,
    @SerialName("subscribergroups")
    val subscriberGroups: List<SubscriberGroupReference> = emptyList(),
    val folderId: String? = null,
    val tags: String? = null,
    val updateDate: Date? = null,
)