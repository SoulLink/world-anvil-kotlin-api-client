package org.soullink.schema.user

import kotlinx.serialization.Serializable


@Serializable
data class UserIdentity(
    val id: String,
    val success: Boolean,
    val username: String,
    val userhash: String
)