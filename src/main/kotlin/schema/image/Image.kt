package org.soullink.schema.image

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.soullink.schema.general.ResourceReference
import org.soullink.schema.general.Date
import org.soullink.schema.general.EntityClass
import org.soullink.schema.general.PrivacyState

@Serializable
data class Image(
    val id: Int,
    val title: String,
    val slug: String?,
    val state: PrivacyState?,
    val isWip: Boolean?,
    val isDraft: Boolean?,
    val entityClass: EntityClass,
    val icon: String?,
    val url: String?,
    @SerialName("subscribergroups") val subscriberGroups: List<ResourceReference>,
    val folderId: String?,
    val tags: String?,
    val updateDate: Date?,
)
