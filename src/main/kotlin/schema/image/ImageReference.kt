package org.soullink.schema.image

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.subscribergroup.SubscriberGroupReference
import org.soullink.schema.general.Date
import org.soullink.schema.general.EntityClass

@Serializable
data class ImageReference(
    val id: Int,
    val title: String,
    val slug: String?,
    val state: PrivacyState?,
    val isWip: Boolean?,
    val isDraft: Boolean?,
    val entityClass: EntityClass,
    val icon: String?,
    val url: String?,
    @SerialName("subscribergroups")
    val subscriberGroups: List<SubscriberGroupReference>,
    val folderId: Int?,
    val tags: String?,
    val updateDate: Date?,
    val isEditable: Boolean = false,
    val success: Boolean = false,
)