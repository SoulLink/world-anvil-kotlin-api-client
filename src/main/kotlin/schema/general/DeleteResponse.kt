package org.soullink.schema.general

import kotlinx.serialization.Serializable

@Serializable
data class DeleteResponse(
    val success: Boolean,
)
