package org.soullink.schema.general

import kotlinx.serialization.Serializable

@Serializable
data class ResourceIntId(
    val id: Int,
)
