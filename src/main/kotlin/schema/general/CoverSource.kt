package org.soullink.schema.general

import kotlinx.serialization.SerialName

enum class CoverSource {
    @SerialName("User")
    USER,
    @SerialName("World")
    WORD,
}