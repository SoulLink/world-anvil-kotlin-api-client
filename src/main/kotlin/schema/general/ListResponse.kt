package org.soullink.schema.general

import kotlinx.serialization.Serializable

@Serializable
data class ListResponse<T>(
    val success: Boolean,
    val entities: List<T>
)


