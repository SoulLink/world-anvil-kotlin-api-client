package org.soullink.schema.general

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class PrivacyState {
    @SerialName("public")
    PUBLIC,
    @SerialName("private")
    PRIVATE,
    @SerialName("Private")
    PRIVATE2,
}
