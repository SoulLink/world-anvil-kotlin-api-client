package org.soullink.schema.general

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class EntityClass {
    @SerialName("World")
    WORLD,
    @SerialName("User")
    USER,
    @SerialName("Image")
    IMAGE,
    @SerialName("BlockTemplate")
    BLOCK_TEMPLATE,
    @SerialName("BlockFolder")
    BLOCK_FOLDER,

    @SerialName("Campaign")
    CAMPAIGN,

    @SerialName("Article")
    ARTICLE,
    @SerialName("Profession")
    PROFESSION,
    @SerialName("Item")
    ITEM,
    @SerialName("Location")
    LOCATION,
    @SerialName("Landmark")
    LANDMARK,
    @SerialName("Organization")
    ORGANIZATION,
    @SerialName("Ritual")
    RITUAL,
    @SerialName("Spell")
    SPELL,
    @SerialName("Material")
    MATERIAL,
    @SerialName("Technology")
    TECHNOLOGY,
    @SerialName("Language")
    LANGUAGE,
    @SerialName("Species")
    SPECIES,
    @SerialName("Rank")
    RANK,
    @SerialName("Settlement")
    SETTLEMENT,
    @SerialName("MilitaryConflict")
    MILITARY_CONFLICT,

    @SerialName("Category")
    CATEGORY,

    @SerialName("Secret")
    SECRET,
    @SerialName("VariableCollection")
    VARIABLE_COLLECTION,
    @SerialName("Manuscript")
    MANUSCRIPT,

    @SerialName("Timeline")
    TIMELINE,
    @SerialName("HistoricalEntry")
    HISTORICAL_ENTRY,
    @SerialName("Map")
    MAP,

    @SerialName("Canvas")
    CANVAS,

    @SerialName("SubscriberGroup")
    SUBSCRIBER_GROUP,
    @SerialName("MembershipType")
    MEMBERSHIP_TYPE,
    @SerialName("Badge")
    BADGE,
}