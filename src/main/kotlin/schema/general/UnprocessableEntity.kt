package org.soullink.schema.general

import kotlinx.serialization.Serializable


@Serializable
data class UnprocessableEntity(
    val success: Boolean,
    val status: String,
    val error: ErrorSummary,
)
