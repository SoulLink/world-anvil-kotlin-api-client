package org.soullink.schema.general

import kotlinx.serialization.Serializable

@Serializable
data class ResourceId(
    val id: String
)
