package org.soullink.schema.general

import kotlinx.serialization.Serializable

@Serializable
data class ListRequestBody(
    val limit: Int,
    val offset: Int,
)