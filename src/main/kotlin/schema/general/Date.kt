package org.soullink.schema.general

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Date(
    val date: String,
    @SerialName("timezone_type") val timezoneType: Int,
    val timezone: String
)