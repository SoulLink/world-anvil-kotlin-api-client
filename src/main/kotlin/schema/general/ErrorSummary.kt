package org.soullink.schema.general

import kotlinx.serialization.Serializable

@Serializable
data class ErrorSummary(
    val summary: String,
    val traceStack: String
)
