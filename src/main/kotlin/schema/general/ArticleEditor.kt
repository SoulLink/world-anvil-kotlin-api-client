package org.soullink.schema.general

import kotlinx.serialization.SerialName

enum class ArticleEditor {
    @SerialName("plutarch")
    PLUTARCH,
}