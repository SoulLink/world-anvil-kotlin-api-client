package org.soullink.schema.general

enum class Granularity(val value: Int) {
    REFERENCE(-1),
    BASE(0),
    ENTITY(1),
    EXTENDED(2),
    ALL(3),
}