package org.soullink.schema.secret

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.general.ResourceId
import org.soullink.shared.Util

@Serializable
data class SecretCreate(
    val title: String,
    val world: ResourceId,
    val author: ResourceId? = null,
    val state: PrivacyState = PrivacyState.PRIVATE,
    val subscriberGroups: List<ResourceId> = emptyList(),
    val tags: String = "",
    val content: String = "",
    val seeded: String = "",
    val cssClasses: String = "",
    val article: ResourceId? = null,
    ) {
    fun toRequestBody(): RequestBody {
        return Json.encodeToString(serializer(), this)
            .toRequestBody(Util.applicationJson)
    }
}
