package org.soullink.schema.secret

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.general.ResourceId
import org.soullink.shared.Util

@Serializable
data class SecretUpdate(
    val title: String? = null,
    val world: ResourceId? = null,
    val author: ResourceId? = null,
    val state: PrivacyState? = null,
    val subscriberGroups: List<ResourceId>? = null,
    val tags: String? = null,
    val content: String? = null,
    val seeded: String? = null,
    val cssClasses: String? = null,
    val article: ResourceId? = null,
    ) {
    fun toRequestBody(): RequestBody {
        return Json.encodeToString(serializer(), this)
            .toRequestBody(Util.applicationJson)
    }
}
