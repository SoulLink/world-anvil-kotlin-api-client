package org.soullink.schema.secret

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.soullink.schema.articles.ArticleReference
import org.soullink.schema.general.Date
import org.soullink.schema.general.EntityClass
import org.soullink.schema.general.PrivacyState
import org.soullink.schema.subscribergroup.SubscriberGroupReference
import org.soullink.schema.user.UserReference
import org.soullink.schema.world.WorldReference

@Serializable
data class SecretExtended(
    val id: String,
    val title: String,
    val slug: String?,
    val state: PrivacyState,
    val isWip: Boolean?,
    val isDraft: Boolean?,
    val entityClass: EntityClass,
    val icon: String,
    val url: String,
    @SerialName("subscribergroups")
    val subscriberGroups: List<SubscriberGroupReference>,
    val folderId: String?,
    val tags: String?,
    val updateDate: Date,

    val content: String?,
    val creationDate: Date,
    val seeded: String?,
    val cssClasses: String?,

    val author: UserReference,
    val article: ArticleReference?,
    val world: WorldReference,
    val editURL: String,

    val isEditable: Boolean,
    val success: Boolean,
)