package org.soullink.endpoints

import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import org.soullink.schema.general.*
import org.soullink.schema.subscribergroup.*
import org.soullink.shared.Util.BASE_URL
import org.soullink.shared.Util.executeRequest

private val logger = KotlinLogging.logger {}

class SubscriberGroupEndpoint(private val httpClient: OkHttpClient) {

    private val path = "subscribergroup"

    fun create(
        title: String,
        worldId: String,
        paidSubscribers: List<String>? = null,
        description: String? = null,
        position: Int? = null,
        isDefault: Boolean? = null,
        isHidden: Boolean? = null,
        isAssignable: Boolean? = null,
    ): SubscriberGroup {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path")
            .put(
                SubscriberGroupCreate(
                    title,
                    ResourceId(worldId),
                    paidSubscribers?.let { ids -> ids.map { ResourceId(it) } },
                    description,
                    position,
                    isDefault,
                    isHidden,
                    isAssignable,
                ).toRequestBody()
            )
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }

    fun update(
        id: String,
        title: String? = null,
        worldId: String? = null,
        paidSubscribers: List<String>? = null,
        description: String? = null,
        position: Int? = null,
        isDefault: Boolean? = null,
        isHidden: Boolean? = null,
        isAssignable: Boolean? = null,
        campaignId: String? = null,
        partyId: String? = null
    ): SubscriberGroupUpdateResponse {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id")
            .patch(
                SubscriberGroupUpdate(
                    title,
                    worldId?.let { ResourceId(it) },
                    paidSubscribers?.let { ids -> ids.map { ResourceId(it) } },
                    description,
                    position,
                    isDefault,
                    isHidden,
                    isAssignable,
                ).toRequestBody()
            )
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }


    fun getRef(id: String): SubscriberGroup {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id&granularity=${Granularity.REFERENCE.value}")
            .get()
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }


    fun getBase(id: String): SubscriberGroupBase {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id&granularity=${Granularity.BASE.value}")
            .get()
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }


    fun get(id: String): SubscriberGroupEntity {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id&granularity=${Granularity.ENTITY.value}")
            .get()
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }


    fun getExtended(id: String): SubscriberGroupExtended {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id&granularity=${Granularity.EXTENDED.value}")
            .get()
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }

    fun delete(id: String): DeleteResponse {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id")
            .delete()
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }
}