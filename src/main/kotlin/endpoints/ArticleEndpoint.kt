package org.soullink.endpoints

import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import org.soullink.schema.general.Granularity
import org.soullink.schema.general.ResourceId
import org.soullink.schema.general.ResourceReference
import org.soullink.schema.articles.Base
import org.soullink.schema.articles.create.CreateGenericArticle
import org.soullink.schema.articles.enums.ArticleTemplate
import org.soullink.schema.articles.update.UpdateGenericArticle
import org.soullink.schema.general.DeleteResponse
import org.soullink.shared.Util.BASE_URL
import org.soullink.shared.Util.executeRequest

class ArticleEndpoint(private val httpClient: OkHttpClient) {

    private val path = "article"
    private val json = Json { classDiscriminator = "entityClass" }


    fun create(worldId: String, title: String, template: ArticleTemplate): ResourceReference {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path")
            .put(
                CreateGenericArticle(
                    title = title,
                    templateType = template,
                    world = ResourceId(worldId),
                    //state = PrivacyState.PUBLIC
                ).toRequestBody()
            )
            .build()

        return executeRequest(request, httpClient) {
            json.decodeFromString(it)
        }
    }

    fun update(id: String, title: String): ResourceReference {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id")
            .patch(
                UpdateGenericArticle(
                    title = title
                ).toRequestBody()
            )
            .build()

        return executeRequest(request, httpClient) {
            json.decodeFromString(it)
        }
    }

    fun get(id: String, granularity: Granularity): Base {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id&granularity=${granularity.value}")
            .get()
            .build()

        return executeRequest(request, httpClient) {
            json.decodeFromString(it)
        }
    }

    fun delete(id: String): DeleteResponse {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id")
            .delete()
            .build()

        return executeRequest(request, httpClient) {
            json.decodeFromString(it)
        }
    }
}
