package org.soullink.endpoints

import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import org.soullink.schema.general.Granularity
import org.soullink.schema.general.ListResponse
import org.soullink.schema.general.ResourceReference
import org.soullink.schema.articles.ArticleReference
import org.soullink.schema.category.CategoryReference
import org.soullink.schema.image.BlockFolderReference
import org.soullink.schema.image.ImageReference
import org.soullink.schema.map.MapReference
import org.soullink.schema.timeline.TimelineReference
import org.soullink.schema.world.World
import org.soullink.shared.Util
import org.soullink.shared.Util.BASE_URL
import org.soullink.shared.Util.executeRequest

private val logger = KotlinLogging.logger {}

class WorldEndpoint(private val httpClient: OkHttpClient) {

    private val path = "world"
    private val articlesPath = "articles"
    private val categoriesPath = "categories"
    private val blockfoldersPath = "blockfolders"
    private val notebooksPath = "notebooks"
    private val secretsPath = "secrets"
    private val variableCollectionsPath = "variablecollections"
    private val subscriberGroupsPath = "subscribergroups"
    private val manuscriptsPath = "manuscripts"
    private val mapsPath = "maps"
    private val imagesPath = "images"
    private val historiesPath = "histories"
    private val timelinesPath = "timelines"
    private val canvasesPath = "canvases"

    fun get(id: String, granularity: Granularity): World {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id&granularity=${granularity.value}")
            .get()
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }

    fun listArticles(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<ArticleReference> {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path/$articlesPath?id=$worldId")
            .post(Util.limitRequest(limit, offset))
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }


    fun listCategories(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<CategoryReference> {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path/$categoriesPath?id=$worldId")
            .post(Util.limitRequest(limit, offset))
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }

    fun listSecrets(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<ResourceReference> = list(
        worldId, secretsPath, limit, offset
    )

    fun listVariableCollections(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<ResourceReference> = list(
        worldId, variableCollectionsPath, limit, offset
    )

    fun listSubscriberGroups(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<ResourceReference> = list(
        worldId, subscriberGroupsPath, limit, offset
    )

    fun listManuscripts(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<ResourceReference> = list(
        worldId, manuscriptsPath, limit, offset
    )

    fun listMaps(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<MapReference> {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path/$mapsPath?id=$worldId")
            .post(Util.limitRequest(limit, offset))
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }

    fun listImages(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<ImageReference> {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path/$imagesPath?id=$worldId")
            .post(Util.limitRequest(limit, offset))
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }

    fun listHistories(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<ResourceReference> = list(
        worldId, historiesPath, limit, offset
    )
    fun listTimelines(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<TimelineReference> {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path/$timelinesPath?id=$worldId")
            .post(Util.limitRequest(limit, offset))
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }

    fun listCanvases(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<ResourceReference> = list(
        worldId, canvasesPath, limit, offset
    )
    fun listBlockFolders(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<BlockFolderReference> {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path/$blockfoldersPath?id=$worldId")
            .post(Util.limitRequest(limit, offset))
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }

    private fun list(worldId: String, slug: String, limit: Int = 50, offset: Int = 0): ListResponse<ResourceReference> {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path/$slug?id=$worldId")
            .post(Util.limitRequest(limit, offset))
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }
}