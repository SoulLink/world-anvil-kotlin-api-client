package org.soullink.endpoints

import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import org.soullink.schema.general.*
import org.soullink.schema.secret.*
import org.soullink.shared.Util.BASE_URL
import org.soullink.shared.Util.executeRequest

private val logger = KotlinLogging.logger {}

class SecretEndpoint(private val httpClient: OkHttpClient) {

    private val path = "secret"
    fun create(worldId: String,
               title: String,
               content: String = "",
               seeded: String = "",
               tags: String = "",
               subscriberGroupIds: List<String> = emptyList(),
               state: PrivacyState = PrivacyState.PRIVATE,
               cssClasses: String = "",
               articleId: String? = null,
               authorId: String? = null,
               ): ResourceReference {
        val body = SecretCreate(
            title = title,
            world = ResourceId(worldId),
            content = content,
            seeded = seeded,
            tags = tags,
            subscriberGroups = subscriberGroupIds.map { ResourceId(it) },
            state = state,
            cssClasses = cssClasses,
            article = articleId?.let { ResourceId(it) },
            author = authorId?.let { ResourceId(it) },
        ).toRequestBody()

        logger.debug { "Creating secret: $body" }

        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path")
            .put(
                body
            )
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }

    fun update(id: String,
               title: String? = null,
               content: String? = null,
               seeded: String? = null,
               tags: String? = null,
               cssClasses: String? = null,
               subscriberGroupIds: List<String>? = null,
               state: PrivacyState? = null,
               articleId: String? = null,
               worldId: String? = null,
               authorId: String? = null,
    ): ResourceReference {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id")
            .patch(
                SecretUpdate(
                    title = title,
                    content = content,
                    state = state,
                    tags = tags,
                    seeded = seeded,
                    subscriberGroups = subscriberGroupIds?.map { ResourceId(it) },
                    cssClasses = cssClasses,
                    article = articleId?.let { ResourceId(it) },
                    world = worldId?.let { ResourceId(it) },
                    author = authorId?.let { ResourceId(it) },
                ).toRequestBody()
            )
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }

    fun getRef(id: String): Secret {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id&granularity=${Granularity.REFERENCE.value}")
            .get()
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }


    fun getBase(id: String): SecretBase {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id&granularity=${Granularity.BASE.value}")
            .get()
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }


    fun getExtended(id: String): SecretExtended {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id&granularity=${Granularity.EXTENDED.value}")
            .get()
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }

    fun delete(id: String): DeleteResponse {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id")
            .delete()
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }
}