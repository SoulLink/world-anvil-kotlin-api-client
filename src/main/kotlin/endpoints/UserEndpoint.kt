package org.soullink.endpoints

import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import okhttp3.RequestBody.Companion.toRequestBody
import org.soullink.schema.blocktemplate.BlockTemplateReference
import org.soullink.schema.general.Granularity
import org.soullink.schema.general.ListResponse
import org.soullink.schema.general.ResourceReference
import org.soullink.schema.user.User
import org.soullink.schema.user.UserIdentity
import org.soullink.schema.user.UserReference
import org.soullink.schema.user.UserUpdate
import org.soullink.schema.world.WorldReference
import org.soullink.shared.Util
import org.soullink.shared.Util.BASE_URL
import org.soullink.shared.Util.executeRequest

private val logger = KotlinLogging.logger {}

class UserEndpoint(private val httpClient: OkHttpClient) {

    private val path = "user"
    private val worldsPath = "worlds"
    private val blockTemplatesPath = "blocktemplates"
    private val notebooksPath = "notebooks"

    fun identity(): UserIdentity {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/identity")
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }

    fun get(id: String, granularity: Granularity): User {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id&granularity=${granularity.value}")
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }

    fun update(id: String, user: UserUpdate): UserReference {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path?id=$id")
            .patch(Json.encodeToString(UserUpdate.serializer(), user).toRequestBody(Util.applicationJson))
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }

    fun listWorlds(userUuid: String, limit: Int = 50, offset: Int = 0): ListResponse<WorldReference> {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path/$worldsPath?id=$userUuid")
            .post(Util.limitRequest(limit, offset))
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }

            Json.decodeFromString(it)
        }
    }

    fun listBlockTemplates(userUuid: String, limit: Int = 50, offset: Int = 0): ListResponse<BlockTemplateReference> {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path/$blockTemplatesPath?id=$userUuid")
            .post(Util.limitRequest(limit, offset))
            .build()

        return executeRequest(request, httpClient) {
            Json.decodeFromString(it)
        }
    }


    fun listNotebooks(worldId: String, limit: Int = 50, offset: Int = 0): ListResponse<ResourceReference> {
        val request = okhttp3.Request.Builder()
            .url("$BASE_URL/$path/$notebooksPath/$worldId")
            .post(Util.limitRequest(limit, offset))
            .build()

        return executeRequest(request, httpClient) {
            logger.debug { it }
            Json.decodeFromString(it)
        }
    }


}
