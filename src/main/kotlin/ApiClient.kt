package org.soullink

import okhttp3.OkHttpClient
import org.soullink.endpoints.*


class ApiClient(
    private val worldAnvilApplicationKey: String,
    private val userAuthenticationToken: String,
) {
    companion object {
        private const val APPLICATION_KEY_HEADER = "x-application-key"
        private const val AUTHORIZATION_TOKEN_HEADER = "x-auth-token"
    }

    private val httpClient = OkHttpClient.Builder()
        .addInterceptor {
            val original = it.request()
            val request = original.newBuilder()
                .addHeader(APPLICATION_KEY_HEADER, worldAnvilApplicationKey)
                .addHeader(AUTHORIZATION_TOKEN_HEADER, userAuthenticationToken)
                .build()
            it.proceed(request)
        }
        .build()

    val user = UserEndpoint(httpClient)
    val world = WorldEndpoint(httpClient)
    val article = ArticleEndpoint(httpClient)
    val secret = SecretEndpoint(httpClient)
    val subscriberGroup = SubscriberGroupEndpoint(httpClient)
}