# World Anvil Kotlin API Client
This is a wrapper library for the World Anvil Boromir API written in Kotlin. Its purpose is to provide a simple way 
to interact with the World Anvil API.

The library is currently in development and is not yet ready for use.

For the latest Boromir API Documentation, please visit:

- [Boromir API Documentation](https://www.worldanvil.com/api/external/boromir/documentation)
- [Boromir API Swagger Documentation](https://www.worldanvil.com/api/external/boromir/swagger-documentation)

## Usage
The library is published to the GitHub Package Registry. To use it, add the following to your `build.gradle.kts` file:

```kotlin
    repositories {
        maven("https://maven.pkg.github.com/WorldAnvil/world-anvil-kotlin-api-client")
    }
```

And add the following to your dependencies:

```kotlin
    dependencies {
        implementation("com.worldanvil:world-anvil-kotlin-api-client:0.1.0")
    }
```

## Publish Package

- https://docs.gitlab.com/ee/user/packages/maven_repository/

